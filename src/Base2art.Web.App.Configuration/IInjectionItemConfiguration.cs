﻿namespace Base2art.Web.App.Configuration
{
    using System;
    using System.Collections.Generic;

    public interface IInjectionItemConfiguration
    {
        Type RequestedType { get; }

        Type FulfillingType { get; }

        IReadOnlyDictionary<string, object> Parameters { get; }

        IReadOnlyDictionary<string, object> Properties { get; }

        bool IsSingleton { get; }
    }
}