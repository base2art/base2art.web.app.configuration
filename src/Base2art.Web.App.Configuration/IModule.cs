namespace Base2art.Web.App.Configuration
{
    using System.Collections.Generic;

    public interface IModule
    {
        IReadOnlyList<IEndpointConfiguration> Endpoints { get; }

        IReadOnlyList<ITypeInstanceConfiguration> GlobalEndpointAspects { get; }

        IReadOnlyList<ITaskConfiguration> Tasks { get; }

        IReadOnlyList<ITypeInstanceConfiguration> GlobalTaskAspects { get; }

        IHealthChecksConfiguration HealthChecks { get; }

        IApplicationConfiguration Application { get; }

        IReadOnlyList<IInjectionItemConfiguration> Injection { get; }

        IReadOnlyList<ICorsItemConfiguration> Cors { get; }
    }
}