﻿namespace Base2art.Web.App.Configuration
{
    using System;
    using System.Linq;

    public class HttpVerb : IEquatable<HttpVerb>
    {
        private static readonly bool[] tokenChars = CreateTokenChars();

        private int hashcode;

        public HttpVerb(string method)
        {
            if (string.IsNullOrEmpty(method))
            {
                throw new ArgumentException("Method Must have Value", nameof(method));
            }

            if (GetTokenLength(method, 0) != method.Length)
            {
                throw new FormatException("Invalid method format");
            }

            this.Method = method;
        }

        public static HttpVerb Get { get; } = new HttpVerb("GET");

        public static HttpVerb Put { get; } = new HttpVerb("PUT");

        public static HttpVerb Post { get; } = new HttpVerb("POST");

        public static HttpVerb Delete { get; } = new HttpVerb("DELETE");

        public static HttpVerb Head { get; } = new HttpVerb("HEAD");

        public static HttpVerb Options { get; } = new HttpVerb("OPTIONS");

        public static HttpVerb Trace { get; } = new HttpVerb("TRACE");

        public string Method { get; }

        public bool Equals(HttpVerb other)
        {
            if ((object) other == null)
            {
                return false;
            }

            if ((object) this.Method == other.Method)
            {
                return true;
            }

            return string.Equals(this.Method, other.Method, StringComparison.OrdinalIgnoreCase);
        }

        public override bool Equals(object obj) => this.Equals(obj as HttpVerb);

        public override int GetHashCode()
        {
            if (this.hashcode == 0)
            {
                this.hashcode = IsUpperAscii(this.Method) ? this.Method.GetHashCode() : this.Method.ToUpperInvariant().GetHashCode();
            }

            return this.hashcode;
        }

        public override string ToString() => this.Method;

        public static bool operator ==(HttpVerb left, HttpVerb right)
        {
            if ((object) left == null)
            {
                return (object) right == null;
            }

            if ((object) right == null)
            {
                return (object) left == null;
            }

            return left.Equals(right);
        }

        public static bool operator !=(HttpVerb left, HttpVerb right) => !(left == right);

        private static bool IsUpperAscii(string value)
            => value.Cast<char>().All(c => c >= 'A' && c <= 'Z');

        internal static int GetTokenLength(string input, int startIndex)
        {
            if (startIndex >= input.Length)
            {
                return 0;
            }

            for (var i = startIndex; i < input.Length; i++)
            {
                if (!IsTokenChar(input[i]))
                {
                    return i - startIndex;
                }
            }

            return input.Length - startIndex;
        }

        internal static bool IsTokenChar(char character)
        {
            if (character > '\u007f')
            {
                return false;
            }

            return tokenChars[character];
        }

// HttpRuleParser
        private static bool[] CreateTokenChars()
        {
            var array = new bool[128];
            for (var i = 33; i < 127; i++)
            {
                array[i] = true;
            }

            array[40] = false;
            array[41] = false;
            array[60] = false;
            array[62] = false;
            array[64] = false;
            array[44] = false;
            array[59] = false;
            array[58] = false;
            array[92] = false;
            array[34] = false;
            array[47] = false;
            array[91] = false;
            array[93] = false;
            array[63] = false;
            array[61] = false;
            array[123] = false;
            array[125] = false;
            return array;
        }
    }
}