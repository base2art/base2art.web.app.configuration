﻿namespace Base2art.Web.App.Configuration
{
    using System;
    using System.Collections.Generic;

    public interface IModelBindingConfiguration
    {
        Type BoundType { get; }

        Type BinderType { get; }

        IReadOnlyDictionary<string, object> BinderParameters { get; }

        IReadOnlyDictionary<string, object> BinderProperties { get; }

        BindingType BindingType { get; }
    }
}