﻿namespace Base2art.Web.App.Configuration
{
    using System.Collections.Generic;

    public interface IEndpointConfiguration : ICallableMethodConfiguration
    {
        HttpVerb Verb { get; }

        string Url { get; }

        IReadOnlyList<ITypeInstanceConfiguration> Aspects { get; }
    }
}