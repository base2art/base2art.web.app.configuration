﻿namespace Base2art.Web.App.Configuration
{
    using System;
    using System.Collections.Generic;

    public interface ICorsItemConfiguration
    {
        bool AllowAnyHeader { get; }

        bool AllowAnyMethod { get; }

        bool AllowAnyOrigin { get; }

        IReadOnlyList<string> ExposedHeaders { get; }

        IReadOnlyList<string> Headers { get; }

        IReadOnlyList<string> Methods { get; }

        IReadOnlyList<string> Origins { get; }

        TimeSpan? PreflightMaxAge { get; }

        bool SupportsCredentials { get; }
    }
}