﻿namespace Base2art.Web.App.Configuration
{
    using System.Collections.Generic;
    using System.Reflection;

    public interface IAssemblyLoader
    {
        IEnumerable<Assembly> Assemblies();
    }
}