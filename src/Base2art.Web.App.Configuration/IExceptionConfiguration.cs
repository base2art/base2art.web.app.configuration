﻿namespace Base2art.Web.App.Configuration
{
    using System.Collections.Generic;

    public interface IExceptionConfiguration
    {
        bool RegisterCommonHandlers { get; }

        bool AllowStackTraceInOutput { get; }

        IReadOnlyList<ITypeInstanceConfiguration> Loggers { get; }
    }
}