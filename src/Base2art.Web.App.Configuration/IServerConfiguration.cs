﻿namespace Base2art.Web.App.Configuration
{
    using System.Collections.Generic;

    public interface IServerConfiguration
    {
        IAssemblyLoader AssemblyLoader { get; }

        string WebRoot { get; }

        IReadOnlyList<IEndpointConfiguration> Endpoints { get; }

        IReadOnlyList<ITaskConfiguration> Tasks { get; }

        IHealthChecksConfiguration HealthChecks { get; }

        IApplicationConfiguration Application { get; }

        IReadOnlyList<IInjectionItemConfiguration> Injection { get; }

        IReadOnlyList<ICorsItemConfiguration> Cors { get; }
    }
}