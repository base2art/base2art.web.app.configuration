﻿namespace Base2art.Web.App.Configuration
{
    public interface IHealthCheckConfiguration : ICallableMethodConfiguration
    {
        string Name { get; }
    }
}