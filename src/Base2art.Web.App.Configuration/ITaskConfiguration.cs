﻿namespace Base2art.Web.App.Configuration
{
    using System;

    public interface ITaskConfiguration : IEndpointConfiguration
    {
        string Name { get; }

        TimeSpan Delay { get; }

        TimeSpan Interval { get; }
    }
}