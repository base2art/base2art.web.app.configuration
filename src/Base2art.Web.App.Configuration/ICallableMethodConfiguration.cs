﻿namespace Base2art.Web.App.Configuration
{
    using System.Reflection;

    public interface ICallableMethodConfiguration : ITypeInstanceConfiguration
    {
        MethodInfo Method { get; }
    }
}