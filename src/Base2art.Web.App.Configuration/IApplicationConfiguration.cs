﻿namespace Base2art.Web.App.Configuration
{
    using System.Collections.Generic;

    public interface IApplicationConfiguration
    {
        IReadOnlyList<ITypeInstanceConfiguration> Filters { get; }

        IReadOnlyList<IModelBindingConfiguration> ModelBindings { get; }

        IReadOnlyDictionary<string, string> MediaTypes { get; }

        IExceptionConfiguration Exceptions { get; }
    }
}