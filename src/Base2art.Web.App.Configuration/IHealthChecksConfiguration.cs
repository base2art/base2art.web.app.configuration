﻿namespace Base2art.Web.App.Configuration
{
    using System.Collections.Generic;

    public interface IHealthChecksConfiguration
    {
        IReadOnlyList<IHealthCheckConfiguration> Items { get; }

        IReadOnlyList<ITypeInstanceConfiguration> Aspects { get; }
    }
}