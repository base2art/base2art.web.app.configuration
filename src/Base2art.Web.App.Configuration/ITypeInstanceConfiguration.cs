﻿namespace Base2art.Web.App.Configuration
{
    using System;
    using System.Collections.Generic;

    public interface ITypeInstanceConfiguration
    {
        Type Type { get; }

        IReadOnlyDictionary<string, object> Parameters { get; }

        IReadOnlyDictionary<string, object> Properties { get; }
    }
}